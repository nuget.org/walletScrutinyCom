---
wsId: 
title: Pera Algo Wallet
altTitle: 
authors: 
users: 100000
appId: com.algorand.android
appCountry: 
released: 2019-06-07
updated: 2022-05-02
version: 5.2.1
stars: 4.4
ratings: 14091
reviews: 1048
size: 61M
website: https://perawallet.app
repository: 
issue: 
icon: com.algorand.android.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2020-12-06
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from:
- /com.algorand.android/

---

