---
wsId: mewEthereum
title: MEW wallet – Ethereum wallet
altTitle: 
authors:
- danny
users: 500000
appId: com.myetherwallet.mewwallet
appCountry: us
released: 2020-03-11
updated: 2022-04-15
version: 2.4.2
stars: 4.3
ratings: 7093
reviews: 513
size: 107M
website: http://mewwallet.com
repository: 
issue: 
icon: com.myetherwallet.mewwallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-02-05
signer: 
reviewArchive: 
twitter: myetherwallet
social:
- https://www.linkedin.com/company/myetherwallet
- https://www.facebook.com/MyEtherWallet
- https://www.reddit.com/r/MyEtherWallet
redirect_from: 

---

Supports 3 chains: Ethereum, Binance and Polygon. 

Does not support BTC.