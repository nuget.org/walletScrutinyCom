---
wsId: 
title: Thunderbolt - Bitcoin and Lightning Wallet
altTitle: 
authors: 
users: 100
appId: com.thunderbolt.wallet
appCountry: 
released: 2021-08-29
updated: 2021-08-29
version: 1.0.3
stars: 
ratings: 
reviews: 
size: 21M
website: https://thunderboltwallet.com
repository: 
issue: 
icon: com.thunderbolt.wallet.png
bugbounty: 
meta: ok
verdict: fake
date: 2021-10-05
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 

---

<div class="alertBox"><div>
<p>This app is <a href="https://github.com/btcontract/wallet/issues/146">most likely a scam!</a></p>
</div></div>