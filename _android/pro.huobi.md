---
wsId: huobi
title: Huobi：Buy Bitcoin, Luna&GMT
altTitle: 
authors:
- leo
users: 5000000
appId: pro.huobi
appCountry: 
released: 2017-11-01
updated: 2022-05-17
version: 6.9.6
stars: 4.8
ratings: 31840
reviews: 493
size: 129M
website: http://www.hbg.com
repository: 
issue: 
icon: pro.huobi.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive: 
twitter: HuobiGlobal
social:
- https://www.facebook.com/huobiglobalofficial
redirect_from:
- /pro.huobi/
- /posts/pro.huobi/

---

Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.
