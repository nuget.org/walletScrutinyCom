---
wsId: ECOS
title: 'ECOS: BTC mining,crypto wallet'
altTitle: 
authors:
- danny
appId: am.ecos.ios.production
appCountry: us
idd: 1528964374
released: 2020-11-25
updated: 2022-04-26
version: 1.24.2
stars: 4.1
reviews: 88
size: '71221248'
website: https://ecos.am/
repository: 
issue: 
icon: am.ecos.ios.production.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive: 
twitter: ecosmining
social:
- https://www.facebook.com/ecosdefi

---

{% include copyFromAndroid.html %}
