---
wsId: AMarkets
title: AMarkets Trading
altTitle: 
authors:
- danny
appId: amarkets.app
appCountry: us
idd: 1495820700
released: 2020-02-12
updated: 2022-05-16
version: 1.4.40
stars: 4.6
reviews: 62
size: '77138944'
website: https://www.amarkets.com/
repository: 
issue: 
icon: amarkets.app.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-13
signer: 
reviewArchive: 
twitter: 
social:
- https://www.linkedin.com/company/amarkets
- https://www.facebook.com/AMarketsFirm

---

{% include copyFromAndroid.html %}
