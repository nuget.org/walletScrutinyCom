---
wsId: keyring
title: 'KEYRING PRO: Wallet Connect'
altTitle: 
authors:
- danny
appId: co.bacoor.keyring
appCountry: 
idd: 1546824976
released: 2021-01-25
updated: 2022-04-30
version: 1.9.0
stars: 3.4
reviews: 5
size: '64482304'
website: 
repository: 
issue: 
icon: co.bacoor.keyring.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-17
signer: 
reviewArchive: 
twitter: KEYRING_PRO
social: 

---

{% include copyFromAndroid.html %}