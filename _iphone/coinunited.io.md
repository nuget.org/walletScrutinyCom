---
wsId: CoinUnited
title: CoinUnited.io - Crypto Wallet
altTitle: 
authors:
- danny
appId: coinunited.io
appCountry: hk
idd: 1565764339
released: 2021-05-14
updated: 2022-05-17
version: 3.3.7
stars: 5
reviews: 334
size: '117361664'
website: https://www.coinunited.io/
repository: 
issue: 
icon: coinunited.io.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-15
signer: 
reviewArchive: 
twitter: realcoinunited
social: 

---

{% include copyFromAndroid.html %}
