---
wsId: ELLIPAL
title: ELLIPAL
altTitle: 
authors:
- leo
appId: com.Ellipal.Ellipal
appCountry: us
idd: 1426179665
released: 2018-08-25
updated: 2022-05-06
version: 3.3.6
stars: 4.8
reviews: 1576
size: '90360832'
website: http://www.ellipal.com/
repository: 
issue: 
icon: com.Ellipal.Ellipal.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-12-03
signer: 
reviewArchive: 
twitter: ellipalwallet
social:
- https://www.facebook.com/ellipalclub
- https://www.reddit.com/r/ELLIPAL_Official

---

{% include copyFromAndroid.html %}