---
wsId: Afriex
title: Afriex - Money transfer
altTitle: 
authors:
- danny
appId: com.afriex.afriex
appCountry: us
idd: 1492022568
released: 2020-03-06
updated: 2022-05-08
version: '11.48'
stars: 4.6
reviews: 720
size: '55657472'
website: https://afriexapp.com
repository: 
issue: 
icon: com.afriex.afriex.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-30
signer: 
reviewArchive: 
twitter: afriexapp
social:
- https://www.linkedin.com/company/afriex
- https://www.facebook.com/AfriexApp

---

{% include copyFromAndroid.html %}
