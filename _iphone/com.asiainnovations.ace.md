---
wsId: ACEexchange
title: ACE Exchange - crypto exchange
altTitle: 
authors:
- danny
appId: com.asiainnovations.ace
appCountry: ng
idd: 1446866556
released: 2019-01-28
updated: 2022-04-27
version: 34.0.0
stars: 0
reviews: 0
size: '65046528'
website: https://www.ace.io
repository: 
issue: 
icon: com.asiainnovations.ace.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-16
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/ace.exchange.tw

---

{% include copyFromAndroid.html %}

