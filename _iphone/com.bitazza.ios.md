---
wsId: bitazza
title: 'Bitazza TH: Crypto Exchange'
altTitle: 
authors:
- danny
appId: com.bitazza.ios
appCountry: th
idd: 1476944844
released: 2020-05-25
updated: 2022-05-18
version: 2.5.5
stars: 4.1
reviews: 833
size: '56370176'
website: https://www.bitazza.com
repository: 
issue: 
icon: com.bitazza.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: bitazzaofficial
social:
- https://www.linkedin.com/company/bitazza
- https://www.facebook.com/bitazza

---

{% include copyFromAndroid.html %}
