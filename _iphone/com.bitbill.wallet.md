---
wsId: ownbit
title: 'Ownbit: Cold & MultiSig Wallet'
altTitle: 
authors:
- leo
appId: com.bitbill.wallet
appCountry: 
idd: 1321798216
released: 2018-02-07
updated: 2022-05-06
version: 4.35.0
stars: 4.4
reviews: 50
size: '124186624'
website: http://www.bitbill.com
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
