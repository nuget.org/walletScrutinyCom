---
wsId: bitkeep
title: 'BitKeep: DeFi Wallet'
altTitle: 
authors:
- leo
appId: com.bitkeep.os
appCountry: 
idd: 1395301115
released: 2018-09-26
updated: 2022-05-12
version: 7.0.7
stars: 4
reviews: 71
size: '81859584'
website: https://bitkeep.com
repository: 
issue: 
icon: com.bitkeep.os.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: BitKeepOS
social:
- https://www.facebook.com/bitkeep
- https://github.com/bitkeepcom

---

 {% include copyFromAndroid.html %}
