---
wsId: bitvavo
title: Bitvavo | Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.bitvavo
appCountry: be
idd: 1483903423
released: 2020-05-28
updated: 2022-04-22
version: 2.0.257
stars: 4.6
reviews: 174
size: '35170304'
website: https://bitvavo.com
repository: 
issue: 
icon: com.bitvavo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
