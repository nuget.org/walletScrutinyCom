---
wsId: BitKan
title: 'BitKan: Trade Bitcoin & Crypto'
altTitle: 
authors:
- danny
appId: com.btckan.us
appCountry: us
idd: 1004852205
released: 2015-06-24
updated: 2022-04-16
version: 8.4.3
stars: 4.7
reviews: 70
size: '178661376'
website: https://bitkan.com/
repository: 
issue: 
icon: com.btckan.us.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: bitkanofficial
social: 

---

{% include copyFromAndroid.html %}
