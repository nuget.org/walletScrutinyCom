---
wsId: bitrue
title: Bitrue
altTitle: 
authors:
- leo
appId: com.cmcm.currency.exchange
appCountry: 
idd: 1435877386
released: 2018-09-16
updated: 2022-05-09
version: 5.2.8
stars: 3.2
reviews: 315
size: '113005568'
website: https://www.bitrue.com
repository: 
issue: 
icon: com.cmcm.currency.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-24
signer: 
reviewArchive: 
twitter: BitrueOfficial
social:
- https://www.facebook.com/BitrueOfficial

---

This app is heavily focused on the "exchange" part which is also in its name.
Nowhere on the App Store can we find claims about self-custody but things like

> - Applies the advanced multi-layer clustered system and the hot/cold wallet
  isolation technology to ensure system security.

only make sense for custodial apps. As a custodial app it is **not verifiable**.
