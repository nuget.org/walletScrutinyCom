---
wsId: coinme
title: 'Coinme: Buy Bitcoin & Crypto'
altTitle: 
authors:
- danny
appId: com.coinme.CoinMe
appCountry: us
idd: 1545440300
released: 2021-05-11
updated: 2022-05-06
version: 1.9.5
stars: 4.6
reviews: 1813
size: '154539008'
website: https://coinme.com/
repository: 
issue: 
icon: com.coinme.CoinMe.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: Coinme
social:
- https://www.linkedin.com/company/coinme
- https://www.facebook.com/Coinme

---

 {% include copyFromAndroid.html %}
