---
wsId: coinsonepaprika
title: 'COINS: One App For Crypto'
altTitle: 
authors:
- danny
appId: com.coinpaprika.Coins
appCountry: us
idd: 1475233621
released: 2019-12-03
updated: 2022-05-12
version: 2.7.4
stars: 4.8
reviews: 838
size: '61978624'
website: http://coins.coinpaprika.com
repository: 
issue: 
icon: com.coinpaprika.Coins.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-09-15
signer: 
reviewArchive: 
twitter: CoinsOneApp
social:
- https://www.facebook.com/CoinsOneApp

---

{% include copyFromAndroid.html %}
