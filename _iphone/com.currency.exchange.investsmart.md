---
wsId: currencycominvesting
title: 'Currency.com: Investing'
altTitle: 
authors:
- danny
appId: com.currency.exchange.investsmart
appCountry: tm
idd: 1499070397
released: 2020-04-14
updated: 2022-05-15
version: 1.25.2
stars: 4.9
reviews: 44
size: '61902848'
website: https://currency.com/
repository: 
issue: 
icon: com.currency.exchange.investsmart.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-01-04
signer: 
reviewArchive: 
twitter: currencycom
social:
- https://www.facebook.com/currencycom
- https://www.reddit.com/r/currencycom

---

{% include copyFromAndroid.html %}
