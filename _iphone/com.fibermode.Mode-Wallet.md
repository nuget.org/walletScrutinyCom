---
wsId: fibermode
title: 'Mode: Buy, Earn & Grow Bitcoin'
altTitle: 
authors:
- danny
appId: com.fibermode.Mode-Wallet
appCountry: gb
idd: 1483284435
released: 2019-11-26
updated: 2022-05-19
version: 5.3.15
stars: 4.3
reviews: 985
size: '44893184'
website: https://www.modeapp.com
repository: 
issue: 
icon: com.fibermode.Mode-Wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: modeapp_
social:
- https://www.linkedin.com/company/modeapp-com
- https://www.facebook.com/themodeapp

---

{% include copyFromAndroid.html %}
