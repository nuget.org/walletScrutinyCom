---
wsId: giottus
title: Giottus
altTitle: 
authors:
- danny
appId: com.giottus.app
appCountry: us
idd: 1537068185
released: 2021-01-27
updated: 2022-04-23
version: 1.0.21
stars: 4.2
reviews: 13
size: '125924352'
website: 
repository: 
issue: 
icon: com.giottus.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-08-27
signer: 
reviewArchive: 
twitter: giottus
social:
- https://www.linkedin.com/company/giottus
- https://www.facebook.com/Giottus

---

{% include copyFromAndroid.html %}
