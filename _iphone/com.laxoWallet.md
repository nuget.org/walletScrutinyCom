---
wsId: VoltWallet
title: 'Volt: Defi & Crypto & Bitcoin'
altTitle: 
authors:
- danny
appId: com.laxoWallet
appCountry: us
idd: 1504656252
released: 2020-04-03
updated: 2022-05-12
version: 2.3.2
stars: 5
reviews: 49
size: '89747456'
website: https://volt.id/
repository: 
issue: 
icon: com.laxoWallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-11
signer: 
reviewArchive: 
twitter: Voltfinance
social: 

---

{% include copyFromAndroid.html %}
