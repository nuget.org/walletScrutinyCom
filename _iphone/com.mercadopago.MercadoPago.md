---
wsId: mercadopago
title: 'Mercado Pago: cuenta digital'
altTitle: 
authors:
- leo
appId: com.mercadopago.MercadoPago
appCountry: br
idd: 925436649
released: 2014-12-17
updated: 2022-05-13
version: 2.221.1
stars: 4.8
reviews: 800147
size: '222577664'
website: http://www.mercadopago.com
repository: 
issue: 
icon: com.mercadopago.MercadoPago.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-12-26
signer: 
reviewArchive: 
twitter: mercadopago
social:
- https://www.facebook.com/mercadopago

---

{% include copyFromAndroid.html %}