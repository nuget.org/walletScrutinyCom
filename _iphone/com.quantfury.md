---
wsId: quantfury
title: Quantfury
altTitle: 
authors:
- danny
appId: com.quantfury
appCountry: gb
idd: 1445564443
released: 2018-12-15
updated: 2022-05-10
version: 1.49.0
stars: 4.6
reviews: 88
size: '55548928'
website: https://quantfury.com/
repository: 
issue: 
icon: com.quantfury.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-01
signer: 
reviewArchive: 
twitter: quantfury
social: 

---

{% include copyFromAndroid.html %}
