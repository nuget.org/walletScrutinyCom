---
wsId: salletone
title: SalletOne Live
altTitle: 
authors:
- danny
appId: com.sallet.walletBit
appCountry: us
idd: 1577493312
released: 2021-07-29
updated: 2022-05-13
version: 1.9.6
stars: 5
reviews: 2
size: '9342976'
website: https://www.salletone.com
repository: 
issue: 
icon: com.sallet.walletBit.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2022-02-17
signer: 
reviewArchive: 
twitter: salletone
social:
- https://www.facebook.com/salletone
- https://github.com/SalletOne

---

{% include copyFromAndroid.html %}
