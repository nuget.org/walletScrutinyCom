---
wsId: Vidulum
title: Vidulum
altTitle: 
authors:
- leo
appId: com.vidulum.app
appCountry: 
idd: 1505859171
released: 2020-07-28
updated: 2022-04-27
version: 1.3.4
stars: 4.6
reviews: 9
size: '68398080'
website: https://vidulum.app
repository: 
issue: 
icon: com.vidulum.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: VidulumApp
social:
- https://www.facebook.com/VidulumTeam
- https://www.reddit.com/r/VidulumOfficial

---

{% include copyFromAndroid.html %}
