---
wsId: 
title: Coinglass - BTC Futures Market
altTitle: 
authors:
- danny
appId: com.xiandanxiaohai.Bybt
appCountry: us
idd: 1522250001
released: 2020-07-08
updated: 2022-04-28
version: 1.4.5
stars: 4.9
reviews: 261
size: '16450560'
website: https://www.coinglass.com
repository: 
issue: 
icon: com.xiandanxiaohai.Bybt.jpg
bugbounty: 
meta: ok
verdict: fake
date: 2021-11-02
signer: 
reviewArchive: 
twitter: coinglass_com
social: 

---

{% include copyFromAndroid.html %}
