---
wsId: DelioLending
title: Delio
altTitle: 
authors:
- danny
appId: io.DelioHybrid
appCountry: kr
idd: 1498891184
released: 2020-02-26
updated: 2022-05-02
version: 1.3.10
stars: 3.2
reviews: 18
size: '72263680'
website: https://www.delio.foundation/
repository: 
issue: 
icon: io.DelioHybrid.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: happydelio
social:
- https://www.facebook.com/delio.io

---

{% include copyFromAndroid.html %}
