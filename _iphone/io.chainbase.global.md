---
wsId: hotbit
title: Hotbit-Global
altTitle: 
authors:
- danny
appId: io.chainbase.global
appCountry: 
idd: 1568969341
released: 2021-05-26
updated: 2022-05-17
version: 1.4.6
stars: 3.5
reviews: 658
size: '49375232'
website: https://www.hotbit.io
repository: 
issue: 
icon: io.chainbase.global.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: Hotbit_news
social:
- https://www.linkedin.com/company/hotbitexchange
- https://www.facebook.com/hotbitexchange

---

{% include copyFromAndroid.html %}
