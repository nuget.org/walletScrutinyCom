---
wsId: getDelta
title: Delta Investment Tracker
altTitle: 
authors: 
appId: io.getdelta.ios
appCountry: us
idd: 1288676542
released: 2017-09-25
updated: 2022-04-21
version: 2022.3.2
stars: 4.7
reviews: 9991
size: '90240000'
website: https://delta.app
repository: 
issue: 
icon: io.getdelta.ios.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
