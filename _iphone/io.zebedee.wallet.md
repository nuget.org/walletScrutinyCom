---
wsId: zebedee
title: 'ZEBEDEE: Play and earn Bitcoin'
altTitle: 
authors:
- leo
appId: io.zebedee.wallet
appCountry: 
idd: 1484394401
released: 2020-11-28
updated: 2022-04-26
version: 2.37.2
stars: 3.5
reviews: 47
size: '96210944'
website: https://zebedee.io
repository: 
issue: 
icon: io.zebedee.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-04-12
signer: 
reviewArchive: 
twitter: zebedeeio
social: 

---

This app is very gamer focused and does no mention at all security aspects or
who's the custodian to your coins:

> The ZEBEDEE Wallet is basically everything you need to start playing games for
  Bitcoin, participating in Bitcoin-powered esports events or collecting Bitcoin
  tips on your live streams.

It is lightning network focused and apparently the counterpart for an sdk the
company is promoting for Bitcoin integration in games.

For lack of a better source I went on [their discord](https://zeb.gg/zebedeeiodiscord)
and asked, so ... according to JC on Discord, this app is custodial. As such it
is **not verifiable**.
