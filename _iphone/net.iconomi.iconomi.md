---
wsId: iconomi
title: 'ICONOMI: Buy and Sell Crypto'
altTitle: 
authors:
- danny
appId: net.iconomi.iconomi
appCountry: si
idd: 1238213050
released: 2017-05-25
updated: 2022-04-20
version: 2.1.6
stars: 4.6
reviews: 79
size: '91932672'
website: http://www.iconomi.com
repository: 
issue: 
icon: net.iconomi.iconomi.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: iconomicom
social:
- https://www.linkedin.com/company/iconominet
- https://www.facebook.com/iconomicom
- https://www.reddit.com/r/ICONOMI

---

{% include copyFromAndroid.html %}
