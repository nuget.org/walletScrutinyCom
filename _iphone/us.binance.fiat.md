---
wsId: BinanceUS
title: 'Binance.US: Buy BTC, ETH, more'
altTitle: 
authors:
- leo
appId: us.binance.fiat
appCountry: 
idd: 1492670702
released: 2020-01-05
updated: 2022-05-12
version: 2.10.1
stars: 4.2
reviews: 99289
size: '222368768'
website: https://www.binance.us/en/home
repository: 
issue: 
icon: us.binance.fiat.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-01-10
signer: 
reviewArchive: 
twitter: binanceus
social:
- https://www.linkedin.com/company/binance-us
- https://www.facebook.com/BinanceUS

---

This is the iPhone version of {% include walletLink.html wallet='android/com.binance.us' %} and we
come to the same conclusion for the same reasons. This app is **not verifiable**.
